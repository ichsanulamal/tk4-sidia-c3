"""sidia_c3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path

#
from .views import index_tn5

# Untuk faskes
from .views import add_faskes_views, list_faskes_views, update_faskes_views, delete_faskes_views, get_lokasi

# Untuk warehouse
from .views import add_warehouse_satgas_views, get_id_lokasi_from_provinsi, list_warehouse_views, update_warehouse_views, delete_warehouse_views 

# Untuk batch pengiriman
from .views import batch_pengiriman_views, create_batch_pengiriman, simpan_daftar_batch_pengiriman


urlpatterns = [
    #Index 
    path('', index_tn5, name='index_tn5'),

    # Faskes
    path('tambah-faskes', add_faskes_views, name='add_faskes'),
    path('list-faskes', list_faskes_views, name='list_faskes'),
    path('faskes/update/<kode_faskes>', update_faskes_views, name='update_faskes_views'),
    path('faskes/delete/<kode_faskes>', delete_faskes_views, name='delete_faskes_views'),
    path('get_lokasi/<id_lokasi>', get_lokasi, name='get_lokasi_API'),

    # Warehouse Satgas
    path('tambah-warehouse-satgas', add_warehouse_satgas_views, name='add_warehouse_satgas_views'),
    path('get_id_lokasi_from_provinsi/<provinsi>', get_id_lokasi_from_provinsi, name='get_id_lokasi_from_provinsi'),
    path('list-warehouse', list_warehouse_views, name='list_warehouse_views'),
    path('warehouse/update/<provinsi>', update_warehouse_views, name='update_warehouse_views'),
    path('warehouse/delete/<kode>', delete_warehouse_views, name='delete_warehouse_views'),
    
    # Batch Pengiriman
    path('batch_pengiriman', batch_pengiriman_views, name='batch_pengiriman_views'),
    path('create_batch_pengiriman/no-transaksi-<no_tsd>', create_batch_pengiriman, name='create_batch_pengiriman'),
    path('simpan_daftar_batch_pengiriman/no-transaksi-<no_tsd>', simpan_daftar_batch_pengiriman, name='simpan_daftar_batch_pengiriman')
    

]
