from django.urls import include, path

# Untuk index
from .views import index_t4

# Untuk kendaraan
from .views import add_kendaraan_views, list_kendaraan_views, update_kendaraan_views, delete_kendaraan_views

# Untuk lokasi
from .views import add_lokasi_views, list_lokasi_views, update_lokasi_views

urlpatterns = [
    # Index
    path('', index_t4, name='index_t4'),

    # Kendaraan
    path('tambah-kendaraan', add_kendaraan_views, name='add_kendaraan'),
    path('list-kendaraan', list_kendaraan_views, name='list_kendaraan'),
    path('kendaraan/update/<nomor>', update_kendaraan_views, name='update_kendaraan_views'),
    path('kendaraan/delete/<nomor>', delete_kendaraan_views, name='delete_kendaraan_views'),

    # Lokasi
    path('tambah-lokasi', add_lokasi_views, name='add_lokasi'),
    path('list-lokasi', list_lokasi_views, name='list_lokasi'),
    path('lokasi/update/<id_lokasi>', update_lokasi_views, name='update_lokasi_views'),
]
