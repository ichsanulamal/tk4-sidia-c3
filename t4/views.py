from decimal import Context
from django.conf.urls import url
from django.db.backends.utils import CursorWrapper
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render, HttpResponseRedirect
from django.db import connection # untuk sql
from django.contrib import messages
import json

"""
Index
"""
def index_t4(request):
    context = {}
    try:
        if request.session['member_id'] != None:
            context['is_authenticated'] = True
            # Buat otoritas tiap role
            if request.session['role_this_user'] == 'ADMIN_SATGAS':
                context['is_admin_satgas'] = True
            if request.session['role_this_user'] == 'SUPPLIER':
                context['is_supplier'] = True
            if request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
                context['is_petugas_distribusi'] = True
            if request.session['role_this_user'] == 'PETUGAS_FASKES':
                context['is_petugas_faskes'] = True

    except:
        pass

    return render(request, 't4/index.html', context)

"""
Nomer 9
"""
def add_kendaraan_views(request):
    context = {}

    if request.method == "POST":
        nomor = request.POST['no_kendaraan']
        nama = request.POST['nama']
        jenis_kendaraan = request.POST['jenis_kendaraan']
        berat_maksimum = request.POST['berat_maks']

        with connection.cursor() as cursor:
            query_insert_kendaraan = "INSERT INTO KENDARAAN VALUES ('{}', '{}', '{}', '{}')" 
            cursor.execute(query_insert_kendaraan.format(nomor, nama, jenis_kendaraan, berat_maksimum))
        return redirect('/t4/list-kendaraan')
    
    try:
        return render(request, 'kendaraan/tambah_kendaraan.html')
    except:
        pass
    
    return redirect('/')

def list_kendaraan_views(request):
    context = {}

    try:
        this_username = request.session['member_id']

        with connection.cursor() as cursor:
            cursor.execute(
            """
            SELECT nomor, nama, jenis_kendaraan, berat_maksimum FROM KENDARAAN"""
            )
            row_all = cursor.fetchall()
        
        context['list_kendaraan'] = row_all
        context['is_authenticated'] = True
        
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            context['is_admin'] = True
        elif request.session['role_this_user'] == 'SUPPLIER':
            context['is_admin'] = False
        else:
            return redirect("/")

        return render(request, 'kendaraan/list_kendaraan.html', context)
    except:
        pass

        return redirect("/")


def update_kendaraan_views(request, nomor):
    context = {}
    context['nomor_vhc'] = nomor

    if request.method == "POST":
        nomor = request.POST['no_kendaraan']
        nama = request.POST['nama']
        jenis_kendaraan = request.POST['jenis_kendaraan']
        berat_maksimum = request.POST['berat_maks']

        with connection.cursor() as cursor:
            query_update_kendaraan = """
            UPDATE KENDARAAN
            SET nama = '{}',
                jenis_kendaraan = '{}',
                berat_maksimum = '{}'
            WHERE nomor = '{}';
            """ 
            cursor.execute(query_update_kendaraan.format(nama, jenis_kendaraan, berat_maksimum, nomor)) 

        return redirect('/t4/list-kendaraan')
    
    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            return render(request, 'kendaraan/edit_kendaraan.html', context)
    except:
        pass

    return redirect("/")

def delete_kendaraan_views(request, nomor):
    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                query_delete = "DELETE FROM KENDARAAN WHERE nomor = '{}';"
                cursor.execute(query_delete.format(nomor))

            messages.info(request, 'Kendaraan dengan nomor {} telah dihapus'.format(nomor))
            return HttpResponseRedirect('/t4/list-kendaraan')
    except:
        pass

    return redirect('/')

"""
Nomer 11
"""
def add_lokasi_views(request):
    context = {}

    if request.method == "POST":
        nomor_id = request.POST['id']
        provinsi = request.POST['provinsi']
        kabkot = request.POST['kab_kota']
        kecamatan = request.POST['kecamatan']
        kelurahan = request.POST['kelurahan']
        jalan_no = request.POST['jalan_no']

        with connection.cursor() as cursor:
            query_insert_lokasi = "INSERT INTO LOKASI VALUES ('{}', '{}', '{}', '{}', '{}', '{}')" 
            cursor.execute(query_insert_lokasi.format(nomor_id, provinsi, kabkot, kecamatan, kelurahan, jalan_no)) 

        return redirect('/t4/list-lokasi')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                cursor.execute("""
                SELECT DISTINCT provinsi 
                FROM LOKASI;
                """)
                list_provinsi = cursor.fetchall()
                context['list_provinsi'] = list_provinsi

                cursor.execute("""
                SELECT id FROM LOKASI;
                """)
                list_id_lokasi = cursor.fetchall()
                context['list_id_lokasi'] = list_id_lokasi
            return render(request, 'lokasi/tambah_lokasi.html', context)
    except:
        pass
    return redirect("/")

def list_lokasi_views(request):
    context = {}
    try:
        this_username = request.session['member_id']

        with connection.cursor() as cursor:
            cursor.execute(
            """
            SELECT id, provinsi, KabKot, Kecamatan, Kelurahan, Jalan_no FROM LOKASI;
            """
            )
            row_all = cursor.fetchall()
        
        context['list_lokasi'] = row_all
        context['is_authenticated'] = True
        
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            context['is_admin'] = True
        elif request.session['role_this_user'] == 'SUPPLIER':
            context['is_admin'] = False
        else:
            return redirect("/")

        return render(request, 'lokasi/list_lokasi.html', context)
    except:
        return redirect("/")

def update_lokasi_views(request, id_lokasi):
    context = {}
    context['id_lokasi'] = id_lokasi

    if request.method == "POST":
        id = request.POST['id']
        provinsi = request.POST['provinsi']
        kabkot = request.POST['kab_kota']
        kecamatan = request.POST['kecamatan']
        kelurahan = request.POST['kelurahan']
        jalan_no = request.POST['jalan_no']
        
        with connection.cursor() as cursor:
            query_update_lokasi = """
            UPDATE LOKASI
            SET provinsi = '{}',
                kabkot = '{}',
                kecamatan = '{}',
                kelurahan = '{}',
                jalan_no = '{}'
            WHERE id = '{}';
            """ 
            cursor.execute(query_update_lokasi.format(provinsi, kabkot, kecamatan, kelurahan, jalan_no, id)) 
        return redirect('/t4/list-lokasi')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':                
           
            with connection.cursor() as cursor:
                cursor.execute("""
                SELECT DISTINCT provinsi 
                FROM LOKASI;
                """)
                list_provinsi = cursor.fetchall()
                context['list_provinsi'] = list_provinsi

                cursor.execute("""
                SELECT id FROM LOKASI;
                """)
                list_id_lokasi = cursor.fetchall()
                context['list_id_lokasi'] = list_id_lokasi

            context['list_provinsi'] = list_provinsi
            context['is_authenticated'] = True
            return render(request, 'lokasi/edit_lokasi.html', context)
    except:
        pass
    return redirect("/")
"""
Nomer 14
"""

def batch_pengiriman_views(request):
    context = {}
    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                query_list_psd = """
                SELECT psdf.No_transaksi_sumber_daya, p.Nama, f.Kode_faskes_nasional, tsd.Total_berat, 
                    rsp.Kode_status_permohonan, rspb.Kode_status_batch_pengiriman
                FROM PERMOHONAN_SUMBER_DAYA_FASKES psdf join PENGGUNA p on psdf.Username_petugas_faskes=p.Username
                    join PETUGAS_FASKES pf on p.Username=pf.Username
                    join FASKES f on pf.Username=f.Username_petugas
                    join TRANSAKSI_SUMBER_DAYA tsd on psdf.No_transaksi_sumber_daya=tsd.Nomor
                    join RIWAYAT_STATUS_PERMOHONAN rsp on psdf.No_transaksi_sumber_daya=rsp.Nomor_permohonan
                    join BATCH_PENGIRIMAN bp on tsd.Nomor=bp.nomor_transaksi_sumber_daya
                    join RIWAYAT_STATUS_PENGIRIMAN rspb on bp.Kode=rspb.Kode_batch
                """
                cursor.execute(query_list_psd)
                row = cursor.fetchall()
                context['list'] = row

                context['is_admin'] = True
                context['is_authenticated'] = True

            return render(request, "batch_pengiriman/list_batch_pengiriman.html", context)
    except:
        pass
    return redirect('/')

def create_batch_pengiriman(request, no_tsd):
    context = {}
    with connection.cursor() as cursor:
        cek_status = """
            SELECT kode_status_permohonan
            FROM RIWAYAT_STATUS_PERMOHONAN 
            WHERE Nomor_permohonan = %s
        """
        cursor.execute(cek_status, no_tsd)
        cek_status = cursor.fetchall()[0][0]
        
        if cek_status == "PRO":
            context['is_processing'] = True
        elif cek_status == 'FIN' or cek_status == 'MAS':
            context['is_processing'] = False
        else:
            return redirect('/')

    if request.method == "POST":
        kode = request.POST['kode']
        petugas_satgas = request.session['member_id']
        petugas_distribusi = request.POST['petugas_distribusi']
        nomor = request.POST['nomor']
        kendaraan = request.POST['kendaraan']

        tanda_terima = str(kode) +"-" + str(nomor)

        with connection.cursor() as cursor:
            query_insert = """
            INSERT INTO BATCH_PENGIRIMAN(Kode, Username_satgas, Username_petugas_distribusi,
                Tanda_terima, nomor_transaksi_sumber_daya, No_kendaraan) VALUES
            (%s, %s, %s, %s, %s, %s)
            """
            cursor.execute(query_insert, [kode, petugas_satgas, petugas_distribusi, \
                tanda_terima, nomor, kendaraan])    

        return redirect('/satgas/create_batch_pengiriman/no-transaksi-{}'.format(no_tsd))

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                petugas_satgas = request.session['member_id']
                context['petugas_satgas'] = petugas_satgas
                context['nomor'] = no_tsd

                query_berat = """
                SELECT tsd.Total_berat 
                FROM PERMOHONAN_SUMBER_DAYA_FASKES psdf 
                    join TRANSAKSI_SUMBER_DAYA tsd on psdf.No_transaksi_sumber_daya=tsd.Nomor
                WHERE No_transaksi_sumber_daya = %s;
                """
                cursor.execute(query_berat, [no_tsd])
                total_berat = cursor.fetchall()
                context['berat'] = total_berat

                query_kode = "select kode from batch_pengiriman"
                cursor.execute(query_kode)
                kode = cursor.fetchall()
                kode = get_kode_baru(kode)
                kode_modif = kode[:3] + str(int(kode[3:])+1)
                context['kode'] = kode_modif

                query_kendaraan = """
                SELECT Nomor, Nama
                FROM KENDARAAN
                WHERE NOT EXISTS 
                    (SELECT rsp.Kode_status_batch_pengiriman 
                        FROM RIWAYAT_STATUS_PENGIRIMAN rsp 
                            join BATCH_PENGIRIMAN bp on rsp.Kode_batch=bp.Kode
                        WHERE bp.No_kendaraan=Nomor and 
                            (rsp.Kode_status_batch_pengiriman='PRO' or rsp.Kode_status_batch_pengiriman='OTW')
                    )
                ;
                """
                cursor.execute(query_kendaraan)
                list_kendaraan = cursor.fetchall()
                context['list_kendaraan'] = list_kendaraan

                query_petugas_distribusi = """
                SELECT pd.username, p.Nama
                FROM PETUGAS_DISTRIBUSI pd 
                    join PENGGUNA p on pd.username=p.username
                WHERE NOT EXISTS 
                    (SELECT rsp.Kode_status_batch_pengiriman 
                        FROM RIWAYAT_STATUS_PENGIRIMAN rsp 
                            join BATCH_PENGIRIMAN bp on rsp.Kode_batch=bp.Kode
                        WHERE bp.Username_petugas_distribusi=pd.username and 
                            (rsp.Kode_status_batch_pengiriman='PRO' or rsp.Kode_status_batch_pengiriman='OTW')
                    )
                ;
                """
                cursor.execute(query_petugas_distribusi)
                list_petugas_distribusi = cursor.fetchall()
                context['list_petugas_distribusi'] = list_petugas_distribusi

                # Bagian tabel daftar batch
                query_daftar_batch = """
                SELECT kode, no_kendaraan, username_petugas_distribusi, 
                    id_lokasi_asal, id_lokasi_tujuan
                FROM BATCH_PENGIRIMAN
                WHERE nomor_transaksi_sumber_daya=%s
                """
                cursor.execute(query_daftar_batch, [no_tsd])
                daftar_batch = cursor.fetchall()
                context['daftar_batch'] = daftar_batch

            context['is_admin'] = True
            context['is_authenticated'] = True
            
            return render(request, 'batch_pengiriman/buat_batch_pengiriman.html', context)
    except:
        pass
    return render('/')

def get_kode_baru(list_kode):
    temp = "pbd0"
    if len(list_kode) == 0:
        return "pbd0"
    for i in list_kode:
        if len(i[0]) > len(temp):
            temp = i[0]
        elif len(i[0]) == len(temp):
            temp = max(i[0], temp)
    return temp

def simpan_daftar_batch_pengiriman(request, no_tsd):
    context = {}
    if request.method == 'POST':
        # Ada trigger nomer 6
        return redirect('/satgas/batch_pengiriman')
    
    return redirect('/')
    

"""

"""