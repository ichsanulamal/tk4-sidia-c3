"""sidia_c3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from . import views
app_name = 'trigger2'

urlpatterns = [
    #Laman utama TrNo 2
    path('', views.index, name='index'),

    #Trigger 3
    path('permohonan', views.permohonan_read, name='permohonan_read'),
    path('permohonan/create', views.permohonan_create, name='permohonan_create'),
    path('permohonan/update/<kode>', views.permohonan_update, name='permohonan_update'),
    path('permohonan/delete/<kode>', views.permohonan_delete, name='permohonan_delete'),

    #Trigger 4
    #path('riwayat', views.permohonan_read, name='permohonan_read'),
    #path('riwayat/create', views.permohonan_create, name='permohonan_create'),

    #Trigger 5
    path('item', views.item_read, name='item_read'),
    path('item/create', views.item_create, name='item_create'),
    path('item/update', views.item_update, name='item_update'),
    path('item/delete/<kode>', views.item_delete, name='item_delete'),
]
